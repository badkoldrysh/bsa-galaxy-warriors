<?php

require __DIR__ . '/vendor/autoload.php';

$reader = new BinaryStudioAcademy\Game\Io\CliReader;
$writer = new BinaryStudioAcademy\Game\Io\CliWriter;
$random = new BinaryStudioAcademy\Game\Helpers\Random;
$spaceshipBuilder = new BinaryStudioAcademy\Game\SpaceshipBuilder\SpaceshipBuilder;

$game = new BinaryStudioAcademy\Game\Game($random, $spaceshipBuilder);

$game->start($reader, $writer);
