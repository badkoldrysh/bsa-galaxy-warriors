<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Helpers;

final class GameParameters
{
    public const SPACE_CRYSTALL = '🔮';
    public const MAGNET_REACTOR = '🔋';
    public const EMPTY_SLOT  = '_';
    public const HOME_GALAXY = 'home';

    public const GALAXIES = [
        'home' => [
            'galaxy' => 'Home Galaxy',
            'spaceship' => 'player'
        ],
        'andromeda' => [
            'galaxy' => 'Andromeda',
            'spaceship' => 'patrol'
        ],
        'pegasus' => [
            'galaxy' => 'Pegasus',
            'spaceship' => 'patrol'
        ],
        'spiral' => [
            'galaxy' => 'Spiral',
            'spaceship' => 'patrol'
        ],
        'shiar' => [
            'galaxy' => 'Shiar',
            'spaceship' => 'battle'
        ],
        'xeno' => [
            'galaxy' => 'Xeno',
            'spaceship' => 'battle'
        ],
        'isop' => [
            'galaxy' => 'Isop',
            'spaceship' => 'executor'
        ]
    ];

    const SPACESHIPS_SCHEMAS = [
        'patrol' => [
            'type' => 'patrol',
            'name' => 'Patrol Spaceship',
            'stats' => [
                'strength' => 3,
                'armor' => 2,
                'luck' => 1,
                'health' => 100,
            ],
            'hold' => [GameParameters::MAGNET_REACTOR, GameParameters::EMPTY_SLOT, GameParameters::EMPTY_SLOT],
        ],
        'battle' => [
            'type' => 'battle',
            'name' => 'Battle Spaceship',
            'stats' => [
                'strength' => 5,
                'armor' => 5,
                'luck' => 3,
                'health' => 100,
            ],
            'hold' => [GameParameters::MAGNET_REACTOR, GameParameters::SPACE_CRYSTALL, GameParameters::EMPTY_SLOT],
        ],
        'executor' => [
            'type' => 'executor',
            'name' => 'Executor',
            'stats' => [
                'strength' => 10,
                'armor' => 10,
                'luck' => 10,
                'health' => 100,
            ],
            'hold' => [GameParameters::MAGNET_REACTOR, GameParameters::MAGNET_REACTOR, GameParameters::SPACE_CRYSTALL],
        ],
        'player' => [
            'type' => 'player',
            'name' => 'Player',
            'stats' => [
                'strength' => 5,
                'armor' => 5,
                'luck' => 5,
                'health' => 100,
            ],
            'hold' => [GameParameters::EMPTY_SLOT, GameParameters::EMPTY_SLOT, GameParameters::EMPTY_SLOT],
        ]
    ];}