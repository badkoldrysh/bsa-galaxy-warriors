<?php

namespace BinaryStudioAcademy\Game\Spaceships;

use BinaryStudioAcademy\Game\Contracts\PlayerSpaceship\PlayerSpaceship as IPlayerSpaceship;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Helpers\Stats;

class PlayerSpaceship extends Spaceship implements IPlayerSpaceship
{
    public function getStats(): array
    {
        return parent::getStats();
    }

    public function increaseStrength(): int
    {
        if (($this->strength + 1) <= Stats::MAX_STRENGTH) {
            $this->strength++;
        }

        return $this->strength;
    }

    public function increaseArmor(): int
    {
        if (($this->armor + 1) <= Stats::MAX_ARMOUR) {
            $this->armor++;
        }

        return $this->armor;
    }

    public function increaseHealth(int $points): int
    {
        if (($this->health + $points) > Stats::MAX_HEALTH) {
            $this->health = Stats::MAX_HEALTH;
            return $this->health;
        }

        $this->health += $points;
        return $this->health;
    }

    public function hasSpaceCrystal(): bool
    {
        return in_array(GameParameters::SPACE_CRYSTALL, $this->hold);
    }

    public function hasEmptySlot(): bool
    {
        return in_array(GameParameters::EMPTY_SLOT, $this->hold);
    }

    public function hasMagnetReactor(): bool
    {
        return in_array(GameParameters::MAGNET_REACTOR, $this->hold);
    }

    public function getMagnetsCount(): int
    {
        $counts = array_count_values($this->hold);

        return $counts[GameParameters::MAGNET_REACTOR] ?? 0;
    }

    public function getHoldCapacity(): int
    {
        $counts = array_count_values($this->hold);

        return $counts[GameParameters::EMPTY_SLOT] ?? 0;
    }
}
