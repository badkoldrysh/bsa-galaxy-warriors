<?php

namespace BinaryStudioAcademy\Game\Spaceships;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship as ISpaceship;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Helpers\Math;
use RuntimeException;

class Spaceship implements ISpaceship
{
    protected $name;  // string name of the spaceship
    protected $type; // string type of the spaceship (player, patrol, battle, executor)
    protected $strength; // int определяет урон, наносимый лазерной установкой (1 до 10)
    protected $armor;   // int уровень защиты, отражаемый при атаке вражеским космическим кораблем (1 - 10)
    protected $luck;     // int определяет удачу попадания в слабозащищенные элементы вражеского корабля (1-10)
    protected $health;   // int определеяет текущий уровень силовой защиты корабля вцелом (1 - 100)
    protected $hold;  // int грузовой отсек корабля для хранения и транспортировки захваченных ресурсов, вместительность: 3 элемента.

    public function __construct()
    {
        $this->name = '';
        $this->type = '';
        $this->strength = 0;
        $this->armor = 0;
        $this->luck = 0;
        $this->health = 0;
        $this->hold = [GameParameters::EMPTY_SLOT, GameParameters::EMPTY_SLOT, GameParameters::EMPTY_SLOT];
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStats(): array
    {
        return [
            'strength' => $this->strength,
            'armor' => $this->armor,
            'luck' => $this->luck,
            'health' => $this->health,
        ];
    }

    public function attack(ISpaceship $oppositeSpaceship, Random $random): int
    {
        $math = new Math;
        $damage = $math->damage($this->strength, $oppositeSpaceship->getArmor()) * $math->luck($random, $this->luck);
        $oppositeSpaceship->decreaseHealth($damage);

        return $damage;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setStrength(int $strength): void
    {
        $this->strength = $strength;
    }

    public function setArmor(int $armor): void
    {
        $this->armor = $armor;
    }

    public function setLuck(int $luck): void
    {
        $this->luck = $luck;
    }

    public function setHealth(int $health): void
    {
        $this->health = $health;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function addToHold(string $item): void
    {
        $item_position = array_search(GameParameters::EMPTY_SLOT, $this->hold);

        if ($item_position === false) {
            throw new RuntimeException("The hold filled");
        }

        unset($this->hold[$item_position]);
        $this->hold[] = $item;
    }

    public function removeFromHold(string $item): void
    {
        $item_position = array_search($item, $this->hold);
        if ($item_position === false) {
            return;
        }

        unset($this->hold[$item_position]);
        $this->hold[] = GameParameters::EMPTY_SLOT;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }
    public function getStrength(): int
    {
        return $this->strength;
    }
    public function getHealth(): int
    {
        return $this->health;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function decreaseHealth(int $points): void
    {
        $points = abs($points);

        if ($this->health - $points < 0) {
            $this->health = 0;
            return;
        }

        $this->health -= abs($points);
    }
}
