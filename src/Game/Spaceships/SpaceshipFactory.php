<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Spaceships;


use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\SpaceshipBuilder\SpaceshipBuilder;
use BinaryStudioAcademy\Game\Contracts\SpaceshipFactory\SpaceshipFactory as ISpaceshipFactory;
use LogicException;

class SpaceshipFactory implements ISpaceshipFactory
{
    private $builder;
    private $director;

    public function __construct(SpaceshipBuilder $builder, Random $random)
    {
        $this->builder = $builder;
        $this->director = new SpaceshipDirector($builder, $random);
    }

    public function getSpaceship(string $name): \BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship
    {
        switch ($name) {
            case 'player':
                $this->director->buildPlayerSpaceship();
                return $this->builder->getSpaceship();
            case 'patrol':
                $this->director->buildPatrolSpaceship();
                return $this->builder->getSpaceship();
            case 'battle':
                $this->director->buildBattleSpaceship();
                return $this->builder->getSpaceship();
            case 'executor':
                $this->director->buildExecutor();
                return $this->builder->getSpaceship();
            default:
                throw new LogicException("Unknown spaceship type");
        }
    }

}