<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Spaceships;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Contracts\SpaceshipBuilder\SpaceshipBuilder;

class SpaceshipDirector
{
    /**
     * @var SpaceshipBuilder
     */
    private $builder;
    private $random;

    public function __construct(SpaceshipBuilder $builder, Random $random)
    {
        $this->random = $random;
        $this->builder = $builder;
    }

    public function buildPatrolSpaceship()
    {
        $schema = GameParameters::SPACESHIPS_SCHEMAS['patrol'];

        $strength = $schema['stats']['strength'] + $this->getRandomInt($this->random, 1);
        $armor = $schema['stats']['armor'] + $this->getRandomInt($this->random, 2);
        $luck = $schema['stats']['luck'] + $this->getRandomInt($this->random, 1);

        $this->builder->setNameAndType($schema['name'], $schema['type']);
        $this->builder->setStats($strength, $armor, $luck, $schema['stats']['health']);
        $this->builder->setHold($schema['hold']);
    }

    public function buildBattleSpaceship()
    {
        $schema = GameParameters::SPACESHIPS_SCHEMAS['battle'];

        $strength = $schema['stats']['strength'] + $this->getRandomInt($this->random, 3);
        $armor = $schema['stats']['armor'] + $this->getRandomInt($this->random, 2);
        $luck = $schema['stats']['luck'] + $this->getRandomInt($this->random, 3);

        $this->builder->setNameAndType($schema['name'], $schema['type']);
        $this->builder->setStats($strength, $armor, $luck, $schema['stats']['health']);
        $this->builder->setHold($schema['hold']);
    }

    public function buildExecutor()
    {
        $schema = GameParameters::SPACESHIPS_SCHEMAS['executor'];

        $this->builder->setNameAndType($schema['name'], $schema['type']);
        $this->builder->setStats($schema['stats']['strength'], $schema['stats']['armor'], $schema['stats']['luck'], $schema['stats']['health']);
        $this->builder->setHold($schema['hold']);
    }

    public function buildPlayerSpaceship()
    {
        $schema = GameParameters::SPACESHIPS_SCHEMAS['player'];

        $this->builder->setNameAndType($schema['name'], $schema['type']);
        $this->builder->setStats($schema['stats']['strength'], $schema['stats']['armor'], $schema['stats']['luck'], $schema['stats']['health']);
        $this->builder->setHold($schema['hold']);
    }

    private function getRandomInt(Random $random, int $modifier): int
    {
        return (int) round($modifier * $random->get());
    }
}