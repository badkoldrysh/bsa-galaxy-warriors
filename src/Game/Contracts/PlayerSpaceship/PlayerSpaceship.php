<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Contracts\PlayerSpaceship;

use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

interface PlayerSpaceship extends Spaceship
{
    public function increaseStrength(): int;

    public function increaseArmor(): int;

    public function increaseHealth(int $points): int;

    public function getMagnetsCount(): int;

    public function hasSpaceCrystal(): bool;

    public function hasEmptySlot(): bool;

    public function hasMagnetReactor(): bool;

    public function getHoldCapacity(): int;
}