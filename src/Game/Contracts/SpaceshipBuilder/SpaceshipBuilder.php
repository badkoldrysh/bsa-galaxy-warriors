<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Contracts\SpaceshipBuilder;


use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

interface SpaceshipBuilder
{
    public function setNameAndType(string $name, string $type): void;
    public function setStats(int $strength, int $armor, int $luck, int $health): void;
    public function setHold(array $hold): void;

    public function getSpaceship(): Spaceship;
}