<?php

namespace BinaryStudioAcademy\Game\Contracts\Spaceship;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

interface Spaceship
{
    public function getStats(): array;

    public function attack(Spaceship $oppositeSpaceship, Random $random): int;

    public function setName(string $name): void;

    public function setStrength(int $strength): void;

    public function setArmor(int $armor): void;

    public function setLuck(int $luck): void;

    public function setHealth(int $health): void;

    public function addToHold(string $item): void;

    public function removeFromHold(string $item): void;
    public function getHold(): array;

    public function getArmor(): int;
    public function getStrength(): int;
    public function getHealth(): int;
    public function getLuck(): int;
    public function getName(): string;
    public function getType(): string;

    public function decreaseHealth(int $points): void;
}
