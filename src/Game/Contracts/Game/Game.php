<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Contracts\Game;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

interface Game
{
    public function start(Reader $reader, Writer $writer): void;

    public function run(Reader $reader, Writer $writer): void;

    public function endGame(): void;

    public function setEnemySpaceship(?Spaceship $spaceship): void;

    public function setCurrentGalaxy(string $galaxyName): void;

    public function getRandom(): Random;

    public function resetGame(): void;

    public function setStuffToGrab(array $hold): void;

    public function resetStuffToGrab(): void;

    public function getCurrentGalaxy(): string;

    public function getStuffToGrab(): array;

    public function popStuffToGrab(): ?string;

    public function winGame(Writer $writer): void;
}
