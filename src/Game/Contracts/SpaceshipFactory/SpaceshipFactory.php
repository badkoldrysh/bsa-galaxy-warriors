<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Contracts\SpaceshipFactory;


use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

interface SpaceshipFactory
{
    public function getSpaceship(string $name): Spaceship;
}