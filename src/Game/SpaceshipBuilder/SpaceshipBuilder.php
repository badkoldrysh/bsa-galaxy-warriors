<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\SpaceshipBuilder;

use BinaryStudioAcademy\Game\Spaceships\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship as ISpaceship;
use BinaryStudioAcademy\Game\Contracts\SpaceshipBuilder\SpaceshipBuilder as ISpaceshipBuilder;
use RuntimeException;

class SpaceshipBuilder implements ISpaceshipBuilder
{
    /**
     * @var Spaceship
     */
    protected $ship;

    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->ship = new Spaceship;
    }

    public function setHold(array $hold): void
    {
        foreach ($hold as $item) {
            try {
                $this->ship->addToHold($item);
            } catch (RuntimeException $e) {
                throw new RuntimeException($e->getMessage());
            }
        }
    }

    public function setNameAndType(string $name, string $type): void
    {
        $this->ship->setName($name);
        $this->ship->setType($type);
    }

    public function setStats(int $strength, int $armor, int $luck, int $health): void
    {
        $this->ship->setStrength($strength);
        $this->ship->setArmor($armor);
        $this->ship->setLuck($luck);
        $this->ship->setHealth($health);
    }

    public function getSpaceship(): ISpaceship
    {
        $ship = $this->ship;
        $this->reset();

        return $ship;
    }
}