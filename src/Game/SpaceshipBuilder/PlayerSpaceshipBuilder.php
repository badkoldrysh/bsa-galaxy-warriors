<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\SpaceshipBuilder;


use BinaryStudioAcademy\Game\Spaceships\PlayerSpaceship;

class PlayerSpaceshipBuilder extends SpaceshipBuilder
{
    public function __construct()
    {
        parent::__construct();
    }

    public function reset(): void
    {
        $this->ship = new PlayerSpaceship();
    }
}