<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Commands\ApplyReactorCommand;
use BinaryStudioAcademy\Game\Commands\AttackCommand;
use BinaryStudioAcademy\Game\Commands\BuyCommand;
use BinaryStudioAcademy\Game\Commands\GrabCommand;
use BinaryStudioAcademy\Game\Commands\RestartCommand;
use BinaryStudioAcademy\Game\Commands\SetGalaxyCommand;
use BinaryStudioAcademy\Game\Commands\StatsCommand;
use BinaryStudioAcademy\Game\Commands\WhereAmICommand;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game as IGame;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\SpaceshipBuilder\PlayerSpaceshipBuilder;

use BinaryStudioAcademy\Game\Commands\ExitCommand;
use BinaryStudioAcademy\Game\Commands\HelpCommand;
use BinaryStudioAcademy\Game\SpaceshipBuilder\SpaceshipBuilder;
use BinaryStudioAcademy\Game\Spaceships\SpaceshipFactory;
use LogicException;

class Game implements IGame
{
    private $random;
    private $stuffToGrab;
    private $enemySpaceship;
    private $currentGalaxy;
    private $player;
    private $endGame;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->endGame = true;

        $this->player = null;
        $this->enemySpaceship = null;
        $this->currentGalaxy = null;

        $this->resetGame();
    }

    public function start(Reader $reader, Writer $writer): void
    {
        $writer->writeln('Galaxy Warriors');
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('Press enter to start... ');
        $reader->read();

        $writer->writeln('Enter a command: ');
        $this->runGameCycle($reader, $writer);
    }

    public function run(Reader $reader, Writer $writer): void
    {
        $input = $reader->read();
        $this->processInputCommand($input, $writer);
    }

    public function endGame(): void
    {
        $this->endGame = true;
    }

    public function setEnemySpaceship(?Spaceship $spaceship): void
    {
        $this->enemySpaceship = $spaceship;
    }

    public function setCurrentGalaxy(string $galaxyName): void
    {
        $this->currentGalaxy = $galaxyName;
    }


    public function getRandom(): Random
    {
        return $this->random;
    }

    public function resetGame(): void
    {
        $this->endGame = false;
        $this->player = (new SpaceshipFactory(new PlayerSpaceshipBuilder(), $this->random))->getSpaceship('player');
        $this->currentGalaxy = 'home';
        $this->resetStuffToGrab();
    }

    public function setStuffToGrab(array $hold): void
    {
        foreach ($hold as $item) {
            if ($item !== GameParameters::EMPTY_SLOT) {
                $this->stuffToGrab[] = $item;
            }
        }
    }

    public function resetStuffToGrab(): void
    {
        $this->stuffToGrab = [];
    }

    public function getCurrentGalaxy(): string
    {
        if ($this->currentGalaxy === null) {
            throw new LogicException("Something is wrong: galaxy is not set.");
        }
        return $this->currentGalaxy;
    }

    public function getStuffToGrab(): array
    {
        return $this->stuffToGrab;
    }

    public function popStuffToGrab(): ?string
    {
        if (empty($this->stuffToGrab)) {
            return null;
        }

        return array_pop($this->stuffToGrab);
    }

    public function winGame(Writer $writer): void
    {
        $writer->writeln('🎉🎉🎉 Congratulations 🎉🎉🎉');
        $writer->writeln('🎉🎉🎉 You are winner! 🎉🎉🎉');

        $this->executeCommand(new RestartCommand($writer, $this));
    }

    private function runGameCycle(Reader $reader, Writer $writer): void
    {
        while (!$this->endGame) {
            $input = trim($reader->read());

            $this->processInputCommand($input, $writer);
        }
    }

    private function processInputCommand(string $input, Writer $writer): void
    {
        $matches = [];
        if (preg_match("#^set-galaxy ([a-zA-Z0-9]+)$#", $input, $matches) > 0) {
            $galaxy = $matches[1];
            $this->executeCommand(new SetGalaxyCommand($writer, $this, new SpaceshipBuilder(), $galaxy));
            return;
        }

        if (preg_match("#^buy ([a-zA-Z0-9]+)$#", $input, $matches) > 0) {
            if ($this->currentGalaxy !== GameParameters::HOME_GALAXY) {
                $writer->writeln("You can buy something just at Home Galaxy.");
                return;
            }

            $itemToBuy = $matches[1];
            $this->executeCommand(new BuyCommand($writer, $this->player, $itemToBuy));
            return;
        }

        switch ($input) {
            case "stats":
                $this->executeCommand(new StatsCommand($writer, $this->player));
                break;
            case "whereami":
                $this->executeCommand(new WhereAmICommand($writer, $this->currentGalaxy));
                break;
            case "exit":
                $this->executeCommand(new ExitCommand($writer, $this));
                break;
            case "help":
                $this->executeCommand(new HelpCommand($writer));
                break;
            case "apply-reactor":
                $this->executeCommand(new ApplyReactorCommand($writer, $this->player));
                break;
            case "attack":
                $this->processAttackCommand($writer);
                break;
            case "grab":
                $this->processGrabCommand($writer);
                break;
            case "restart":
                $this->executeCommand(new RestartCommand($writer, $this));
                break;
            default:
                $writer->writeln("Command '$input' not found");
                break;
        }
    }

    private function processAttackCommand(Writer $writer): void
    {
        if ($this->currentGalaxy === GameParameters::HOME_GALAXY ||
            $this->enemySpaceship === null) {
            $writer->writeln("Calm down! No enemy spaceships detected. No one to fight with.");
            return;
        }

        $this->executeCommand(new AttackCommand($writer, $this->player, $this->enemySpaceship, $this));
    }

    private function processGrabCommand(Writer $writer): void
    {
        if ($this->currentGalaxy === GameParameters::HOME_GALAXY) {
            $writer->writeln("Hah? You don't want to grab any staff at Home Galaxy. Believe me.");
            return;
        }

        if ($this->enemySpaceship !== null){
            $writer->writeln("LoL. Unable to grab goods. Try to destroy enemy spaceship first.");
            return;
        }

        if (empty($this->stuffToGrab)) {
            $writer->writeln("There is no stuff to grab. Try everywhere else.");
            return;
        }

        $this->executeCommand(new GrabCommand($writer, $this->player, $this));
    }

    private function executeCommand(Command $command): void
    {
        $command->execute();
    }
}
