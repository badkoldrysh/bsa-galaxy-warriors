<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;

class StatsCommand implements Command
{
    private $writer;
    private $spaceship;

    public function __construct(Writer $writer, Spaceship $spaceship)
    {
        $this->writer = $writer;
        $this->spaceship = $spaceship;
    }

    public function execute(): void
    {
        $stats = $this->spaceship->getStats();
        $this->writer->writeln("Spaceship stats:");

        foreach ($stats as $key => $value) {
            $this->writer->writeln($key . ": " . $value);
        }

        $this->showHold($this->spaceship->getHold());
    }

    private function showHold(array $hold): void
    {
        $show_string = 'hold: [ ';
        foreach ($hold as $item) {
            $show_string .= $item . ' ';
        }
        $show_string .= ']';

        $this->writer->writeln($show_string);
    }
}