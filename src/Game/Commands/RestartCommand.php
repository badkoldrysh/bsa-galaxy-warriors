<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class RestartCommand implements Command
{
    private $writer;
    private $game;

    public function __construct(Writer $writer, Game $game)
    {
        $this->writer = $writer;
        $this->game = $game;
    }

    public function execute(): void
    {
        $this->game->resetGame();
        $this->writer->writeln("Game has been restarted. You are at Home Galaxy now.");
    }
}