<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\PlayerSpaceship\PlayerSpaceship;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Helpers\Stats;
use LogicException;

class BuyCommand implements Command
{
    private $writer;
    private $spaceship;
    private $item;

    public function __construct(Writer $writer, PlayerSpaceship $spaceship, string $item)
    {
        $this->writer = $writer;
        $this->spaceship = $spaceship;
        $this->item = $item;
    }

    public function execute(): void
    {
        if (!in_array($this->item, ['strength', 'armor', 'reactor'])) {
            $this->writer->writeln("Unknown object. You can buy strength, armor or reactor.");
            return;
        }

        if (!$this->spaceship->hasSpaceCrystal()) {
            $this->writer->writeln("You don't have enough space crystal. Find a real job.");
            return;
        }

        $nextValue = 0;
        if ($this->item == 'strength') {
            if ($this->spaceship->getStats()['strength'] == Stats::MAX_STRENGTH) {
                $this->writer->writeln("You have maximal strength level.");
                return;
            }

            $this->spaceship->removeFromHold(GameParameters::SPACE_CRYSTALL);
            $nextValue = $this->spaceship->increaseStrength();
        } else if ($this->item == 'armor') {
            if ($this->spaceship->getStats()['armor'] == Stats::MAX_ARMOUR) {
                $this->writer->writeln("You have maximal armor level.");
                return;
            }

            $this->spaceship->removeFromHold(GameParameters::SPACE_CRYSTALL);
            $nextValue = $this->spaceship->increaseArmor();
        } else if ($this->item == 'reactor') {
            if (!$this->spaceship->hasEmptySlot()) {
                $this->writer->writeln("Your hold is filled.");
                return;
            }

            $this->spaceship->removeFromHold(GameParameters::SPACE_CRYSTALL);
            $this->spaceship->addToHold(GameParameters::MAGNET_REACTOR);
            $nextValue = $this->spaceship->getMagnetsCount();
        }

        $message = $this->item == 'reactor'
            ? "You've bought a magnet reactor. You have {$nextValue} reactor(s) now."
            : "You've got upgraded skill: {$this->item}. The level is {$nextValue} now.";

        $this->writer->writeln($message);

        if (count($this->spaceship->getHold()) > 3) {
            throw new LogicException("The player's hold is too big. Obviously something is wrong");
        }
    }
}