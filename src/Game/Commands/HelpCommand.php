<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class HelpCommand implements Command
{
    private $writer;

    private const HELP_COMMANDS = [
        'List of commands:',
        'help - shows this list of commands',
        'stats - shows stats of spaceship',
        'set-galaxy <home|andromeda|spiral|pegasus|shiar|xeno|isop> - provides jump into specified galaxy',
        'attack - attacks enemy\'s spaceship',
        'grab - grab useful load from the spaceship',
        'buy <strength|armor|reactor> - buys skill or reactor (1 item)',
        'apply-reactor - apply magnet reactor to increase spaceship health level on 20 points',
        'whereami - shows current galaxy',
        'restart - restarts game',
        'exit - ends the game',
    ];

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute(): void
    {
        foreach (self::HELP_COMMANDS as $line) {
            $this->writer->writeln($line);
        }
    }
}
