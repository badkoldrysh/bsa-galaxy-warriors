<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ExitCommand implements Command
{
    private $game;
    private $writer;

    public function __construct(Writer $writer, Game $game)
    {
        $this->writer = $writer;
        $this->game = $game;
    }

    public function execute(): void
    {
        $this->game->endGame();
        $this->writer->writeln("Really? Ok, so bye-bye.");
    }
}
