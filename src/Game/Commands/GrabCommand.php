<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\PlayerSpaceship\PlayerSpaceship;

class GrabCommand implements Command
{
    private $writer;
    private $playerSpaceship;
    private $game;

    public function __construct(Writer $writer, PlayerSpaceship $playerSpaceship, Game $game)
    {
        $this->writer = $writer;
        $this->playerSpaceship = $playerSpaceship;
        $this->game = $game;
    }

    public function execute(): void
    {
        if ($this->playerSpaceship->getHoldCapacity() === 0) {
            $this->writer->writeln("You don't have empty slots for grab this items.");
            return;
        }

        $message = "You got";
        while (($item = $this->game->popStuffToGrab()) !== null) {
            $this->playerSpaceship->addToHold($item);
            $message .= ' ' . $item;

            if (!$this->playerSpaceship->hasEmptySlot()) {
                $this->writer->writeln("Your hold is full.");
                break;
            }
        }

        $message .= ".";
        $this->writer->writeln($message);
    }
}