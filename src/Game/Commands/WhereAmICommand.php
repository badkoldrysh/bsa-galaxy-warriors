<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use LogicException;

class WhereAmICommand implements Command
{
    private $writer;
    private $galaxy;

    public function __construct(Writer $writer, string $galaxy)
    {
        $this->writer = $writer;

        if (!isset(GameParameters::GALAXIES[$galaxy])) {
            throw new LogicException("Galaxy isn't in galaxies list. Obviosly something is wrong");
        }
        $this->galaxy = GameParameters::GALAXIES[$galaxy];
    }

    public function execute(): void
    {
        $this->writer->writeln("Galaxy: " . $this->galaxy['galaxy']);
    }
}