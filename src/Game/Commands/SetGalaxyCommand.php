<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;
use BinaryStudioAcademy\Game\Contracts\SpaceshipBuilder\SpaceshipBuilder;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Spaceships\SpaceshipFactory;

class SetGalaxyCommand implements Command
{
    private $writer;
    private $game;
    private $galaxy;
    private $factory;

    public function __construct(Writer $writer, Game $game, SpaceshipBuilder $builder, string $galaxy)
    {
        $this->factory = new SpaceshipFactory($builder, $game->getRandom());
        $this->writer = $writer;
        $this->game = $game;
        $this->galaxy = $galaxy;
    }

    public function execute(): void
    {
        // TODO: Where is player? Maybe player is already in this galaxy
        if (!isset(GameParameters::GALAXIES[$this->galaxy])) {
            $this->writer->writeln("Nah. No specified galaxy found.");
            return;
        }

        $galaxyInfo = GameParameters::GALAXIES[$this->galaxy];
        $this->game->setCurrentGalaxy($this->galaxy);
        $this->game->resetStuffToGrab();
        $this->writer->writeln("Galaxy: " . $galaxyInfo['galaxy'] . ".");

        if ($galaxyInfo['galaxy'] === GameParameters::GALAXIES['home']['galaxy']) {
            $this->game->setEnemySpaceship(null);
            return;
        }

        $localSpaceship = $this->factory->getSpaceship($galaxyInfo['spaceship']);
        $this->game->setEnemySpaceship($localSpaceship);
        $this->showLocalSpaceship($localSpaceship);
    }

    private function showLocalSpaceship(Spaceship $localSpaceship): void
    {
        $this->writer->writeln("You see a " . $localSpaceship->getName() . ": ");

        foreach ($localSpaceship->getStats() as $key => $value) {
            $this->writer->writeln($key . ": " . $value);
        }
    }
}