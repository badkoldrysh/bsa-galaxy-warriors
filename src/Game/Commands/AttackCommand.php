<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Game\Game;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\PlayerSpaceship\PlayerSpaceship;
use BinaryStudioAcademy\Game\Contracts\Spaceship\Spaceship;
use BinaryStudioAcademy\Game\Helpers\GameParameters;

class AttackCommand implements Command
{
    private $game;
    private $writer;
    private $playerSpaceship;
    private $enemySpaceship;

    public function __construct(Writer $writer, PlayerSpaceship $playerSpaceship, Spaceship $enemySpaceship, Game $game)
    {
        $this->game = $game;
        $this->writer = $writer;
        $this->playerSpaceship = $playerSpaceship;
        $this->enemySpaceship = $enemySpaceship;
    }

    public function execute(): void
    {
        $playerDamage = $this->playerSpaceship->attack($this->enemySpaceship, $this->game->getRandom());

        if ($this->enemySpaceship->getHealth() <= 0) {
            $this->writer->writeln("{$this->enemySpaceship->getName()} is totally destroyed. Hurry up! There is could be something useful to grab.");

            if ($this->enemySpaceship->getType() === GameParameters::SPACESHIPS_SCHEMAS['executor']['type']) {
                $this->game->winGame($this->writer);
            }

            $this->game->setEnemySpaceship(null);
            $this->game->setStuffToGrab($this->enemySpaceship->getHold());

            return;
        }

        $enemyDamage = $this->enemySpaceship->attack($this->playerSpaceship, $this->game->getRandom());

        if ($this->playerSpaceship->getHealth() <= 0) {
            $this->writer->writeln("Your spaceship got significant damages and eventually got exploded.");
            $this->writer->writeln("You have to start from Home Galaxy.");
            $this->game->resetGame();
            return;
        }

        $this->writer->writeln("{$this->enemySpaceship->getName()} has damaged on: {$playerDamage} points.");
        $this->writer->writeln("health: {$this->enemySpaceship->getHealth()}");

        $this->writer->writeln("{$this->enemySpaceship->getName()} damaged your spaceship on: {$enemyDamage} points.");
        $this->writer->writeln("health: {$this->playerSpaceship->getHealth()}");
    }
}
