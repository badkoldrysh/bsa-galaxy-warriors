<?php

declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\PlayerSpaceship\PlayerSpaceship;
use BinaryStudioAcademy\Game\Helpers\GameParameters;
use BinaryStudioAcademy\Game\Helpers\Stats;

class ApplyReactorCommand implements Command
{
    private $writer;
    private $playerSpaceship;
    private const HEALTH_POINTS = 20;

    public function __construct(Writer $writer, PlayerSpaceship $playerSpaceship)
    {
        $this->writer = $writer;
        $this->playerSpaceship = $playerSpaceship;
    }

    public function execute(): void
    {
        if (!$this->playerSpaceship->hasMagnetReactor()) {
            $this->writer->writeln("You don't have Magnet reactor to apply.");
            return;
        }

        if ($this->playerSpaceship->getStats()['health'] >= Stats::MAX_HEALTH) {
            $this->writer->writeln("You can't applied Magnet reactor. Youre spaceship's health is full.");
            return;
        }

        $this->playerSpaceship->removeFromHold(GameParameters::MAGNET_REACTOR);
        $health = $this->playerSpaceship->increaseHealth(self::HEALTH_POINTS);

        $this->writer->writeln("Magnet reactor have been applied. Current spaceship health level is {$health}");
    }
}